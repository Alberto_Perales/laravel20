<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function hola(){
        return "Hola desde PruebaController";
    }
    public function saludoCompleto($name){
        return "hola $name. Encantado de conocerle";
    }

    public function saludo(Request $request){

        $informal = $request->input('informal');
        $numeros = [1,2,3,4];
        $informal = $request->informal;
        $all = $request->all();
        /*if ($informal){
            echo "Hola";
        } else{
            echo "Buenos días";
        }*/
        return view('prueba.saludo', [
            'informal' => $informal, //a la vista hay que pasarle las variables, mediante un array
            'numeros' => $numeros
            ]);

        //va a buscar un fichero en el directorio views:
        //exixte prueba/saludo.blade.php ??
        //existe prueba/saludo.php ??
    }


    public function tablaSaludo(){
        echo "Tabla";
    }
    public function tabla(Request $request){
        $size = $request->input('size');

        return view('prueba.tabla',[
            'size' => $size
        ]);


    }
}
