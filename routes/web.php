<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\StudyController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('prueba', function(){
    echo "hola mundo";
});

Route::get('/prueba2', [PruebaController::class, 'hola']); //hola es el método
Route::get('/prueba2/{name}', [PruebaController::class, 'saludoCompleto']); //hola es el método


Route::resource('photos', PhotoController::class); // Te crea las siete de abajo
Route::resource('studies', StudyController::class); // Te crea las siete de abajo

Route::get('saludo', [PruebaController::class, 'saludo']);

Route::get('tabla', [PruebaController::class, 'tablaSaludo']);
Route::get('tabla/{size}', [PruebaController::class, 'tabla']);


/* ESTO ES ELO MISMO
Route::get('photos/create', [PhotoController::class, 'create']);
Route::post('photos', [PhotoController::class, 'store']);
Route::get('photos', [PhotoController::class, 'index']);
Route::get('photos/{id}', [PhotoController::class, 'show']);
Route::get('photos/{id}/edit', [PhotoController::class, 'edit']);
Route::put('photos/{id}', [PhotoController::class, 'update']);
Route::delete('photos/{id}', [PhotoController::class, 'destro']);
*/

// rutas -> plural
// tablas-> plural
// controladores y modelos -> singular
