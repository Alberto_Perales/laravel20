<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>

    @if ($informal)
    Hola MUndo!
    
    @else
        Buenos días mundo!  
    @endif
</h1>
    <!--<?php echo $informal?> ESTO EN LARAVEL NO-->
    {{$informal}} <!--ESTO ES BLADE, EL GESTOR DE PLANTILLAS DE LARAVEL-->

    <h1>Lista de números</h1>
    @foreach ($numeros as $numero)
        <li>Número: {{$numero}}</li>
    @endforeach

</body>
</html> 